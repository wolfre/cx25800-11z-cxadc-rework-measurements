#!/bin/bash

name=$1

function die
{
	echo "$@"
	exit 1
}


if [[ ! -f "$name" ]] ; then
	die Need file
fi


fmt=""
if [[ "$name" == *.u16 ]] ; then
	fmt=u16le
else
	fmt=u8
fi

rate_khz=$(echo $name | sed "s/capture-\(.*\)khz.*/\1/")

ffmpeg -f $fmt -ar ${rate_khz}k -ac 1 -i $name ${name}.wav
