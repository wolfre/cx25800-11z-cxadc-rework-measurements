# CX25800-11Z CXADC rework and measurements

To capture some RF on the cheap side I started looking into [CXADC][cxadc] and bought 2 "white PCIe capture cards" in July 2023.
Both cards I got are based on a Connexant CX25800-11z, and this document covers the steps I did to modify them and the measurements I did to check on their performance.
Any references here to "the data sheet" are taken from "CX25800 PCI Video Decoder with Mono Audio Input Data Sheet" - "DSH-201233A July 2007" - "Revision A July 2, 2007" ([google](https://www.google.com/search?q=DSH-201233A)).
The two cards have the following markings on the Connexant CX25800-11z chip *1329Y1Z2 / 1329* and *1227Y1CH / 1228* and these on silk screen *VT610EX Rev:SC3A VTImage 19.07*.
The data sheet does not explain them, but they could be read as date codes: *29th week of 2013* and *28th week of 2012* respectively.

**Index**

- [Hardware mod](#hardware-mod)
- [Performance measurements with stock 28.6 MHz crystal](#performance-measurements-with-stock-286-mhz-crystal)
  - [Setup](#setup)
  - [Results](#results)
  - [Conclusions / summary with stock 28.6 MHz crystal](#conclusions-summary-with-stock-286-mhz-crystal)
- [Performance measurements with clock generator at 20, 40 and 50 MHz](#performance-measurements-with-clock-generator-at-20-40-and-50-mhz)
  - [Silence / noise floor](#silence-noise-floor)
  - [Sine 10kHz](#sine-10khz)
  - [Conclusions / summary with clock generator at 20, 40 and 50 MHz](#conclusions-summary-with-clock-generator-at-20-40-and-50-mhz)
- [10 Bit "Filtered VBI data"](#10-bit-filtered-vbi-data)
- [CXADC `center_offset`](#cxadc-center_offset)
- [CXADC `level`](#cxadc-level)
- [CXADC `tenxfsc`](#cxadc-tenxfsc)
- [A picture of the acquisition system](#a-picture-of-the-acquisition-system)
- [THD measurements with a PC sound card](#thd-measurements-with-a-pc-sound-card)
- [Future stuff](#future-stuff)
- [Abbreviations](#abbreviations)

## Hardware mod

The CX25800-11Z is very similar to the [more common CX23883-39](https://github.com/happycube/cxadc-linux3/wiki/Types-Of-CX2388x-Cards), with the same pinout.
However they lack (according to their data sheet) the S-Video capabilities and pin 150 is marked as *reserved*.
The cards I got however still route the chroma line from the S-Video din connector to that (reserved) pin?
I did not test whether this actually works or not.

![input-stage-original.png](input-stage-original.png)

The PCIe card layout features 3 identical input stages from the connectors (e.g. RCA jack) to the VMUX inputs on the CX25800.
The above diagram shows one of these input stages (component names are deliberately placeholders and do not appear on silkscreen).
The inductor (Lf) and first capacitor (Cf1) form a low-pass filter, possibly for anti-aliasing, to improve performance for composite signals.
The 75 Ohms (Rt) is there for termination of course and 3.3µ (Cf2) is a DC block towards the VMUX / ADC input.
Putting the 75 Ohms before the DC block is per CX25800-11Z data sheet (3.1.2.1 ADCs / Figure 65), but the additional low-pass filter is not specifically described in there (I also did not measure the values of Lf and Cf1).
If the signals you want to capture are limited to composite signals then keeping the input stages as they are is probably fine.
To capture the full bandwidth, Lf and Cf1 need to be removed.

I targeted the top (unpopulated) input of the cards, which routes to the VMUX1 input.
*NOTE* the [CXADC][cxadc] driver starts counting at 0, the data sheet starts at 1, all references in this document refer to the data sheet numbering.
For this input, the mapping is like this:
- Lf = L11
- Cf1 = C58
- Cf2 = C16
- Rt = R25

L11 and C58 need to be removed, but for some reason C16 is unpopulated.
Instead they populated C31 right next to it, which bridges VMUX1 and VMUX2.
So remove C31 and place it into the C16 position.
Finally install a BNC connector / socket (or any other connector you wish), and use the former L11 footprint to solder the signal to.

![card-modded.jpg](card-modded.jpg)

The above picture shows the end result, with a custom 3D printed bracket.
The picture also shows the S-Video din connector removed, to make the bracket more stable and because I did not need it anyways.
But that is entirely optional and the din connector can be left where it is.

## Performance measurements with stock 28.6 MHz crystal

Before capturing my actual target signals, I wanted to get an idea how good the setup / mod was.
And also how much the different sampling configurations impacted the performance.

### Setup

![measurement-setup.png](measurement-setup.png)

The above diagram shows the measurement setup: a Joy-IT JDS2915 function generator connected to the (BNC) input of the CX25800 card, and a T connector from the card input to a Rigol DS1054Z.
The function generator was used to generate a sine wave at a specific frequency and level.
The DSO was used verify the exact input level at the card, and to monitor the base distortion of the generator (which limits the precision on the following results).
For each point at least 1 second of signal was captured with [capture.sh](capture.sh) (setting the `FSCX` and `TEN` parameters respectively).
Then the result was converted to a wav with [capture-to-wav.sh](capture-to-wav.sh) and loaded into audacity to analyze with the convenient "plot spectrum" function.

NOTE1: THD calculation was simplified by going for the 1st harmonic only, which should be a reasonable approximation.

NOTE2: Was unable to get averaging on FFT plot to work in DSO, so account for about 2-4 dB tolerance on DSO THD values.

NOTE3: sample rates are calculated like this
- 10Bit 14.3Msps = 28636360Hz * 1.00 / 2
- 10Bit 17.7Msps = 28636360Hz * 1.24 / 2
- 10Bit 20.0Msps = 28636360Hz * 1.40 / 2
- 8Bit 28.6Msps = 28636360Hz * 1.00 / 1
- 8Bit 35.5Msps = 28636360Hz * 1.24 / 1

### Results

All below results were on the card with the *1329Y1Z2 / 1329* CX25800-11Z, and via BNC on VMUX1, filters removed as previously described.
Also CX25800 internal +6 Db analog gain was disabled and the gain of the additional CX25800 internal ADC preamplifier (AGC block in data sheet) was set to 0 (minimum).

**Frequency tests:**

| Signal / Amplitude | THD DSO | THD 14.3Msps 10Bit | THD 17.7Msps 10Bit | THD 28.6Msps 8Bit | THD 35.5Msps 8Bit |
|--------------------|---------|--------------------|--------------------|-------------------|-------------------|
|  10KHz @  100mVpp  | -43 dB  | -43.8 dB           | n/a                | n/a               | -41.6 dB          |
|  10kHz @  250mVpp  | -43 dB  | -42.3 dB           | n/a                | n/a               | n/a               |
|  10kHz @ 1000mVpp  | -45 dB  | -41.5 dB           | -41.4 dB           | n/a               | n/a               |
|  10kHz @ 1500mVpp  | -45 dB  | -41.2 dB           | n/a                | n/a               | n/a               |
|  10kHz @ 2000mVpp  | -42 dB  | -40.3 dB           | n/a                | n/a               | n/a               |
|                    |         |                    |                    |                   |                   |
| 250kHz @ 1500mVpp  | -47 dB  | -40.7 dB           | n/a                | -40.7 dB          | n/a               |

**DC bias:**

![dc-offset-10bit-1329Y1Z2.png](dc-offset-10bit-1329Y1Z2.png)

The above image shows the 10KHz at 2000mV peak-peak input capture.
Clearly visible is the DC center / offset is not at the mid-point between ADC min/max values.
This effectively limits the analog input voltage range, 2000mVpp being about 90% of the usable range.
So when designing or adapting input signals to this card, 2000mVpp seems a good maximum.

**Noise floor:**

![noise-floor-1329Y1Z2.png](noise-floor-1329Y1Z2.png)

The above image shows the spectrum of a capture at 14.3Msps 10Bit, with the BNC input of the card terminated into an appropriate resistor.
The following interesting peaks are visible:
- 1.06Mhz -72.1dB FS
- 2.14Mhz -75.0dB FS
- 4.77Mhz -60.6dB FS
  - This peak is 1/6th of the crystal and could be some form of crosstalk / coupling: 28636360Hz / 6 = 4.772727MHz

So the noise floor limits the usable dynamic range to about -60 dB FS.

### Conclusions / summary with stock 28.6 MHz crystal

The THD measured at the card input / signal generator output shows about the same THD as the capture signal in the recording.
This means it is not clearly visible where the distortion comes from: either the signal generator or the CX25800.
On top of that, the FFT function in the DSO used did not offer averaging and so it was difficult to determine the input THD precisely.
The results from the frequency tests should be repeated with a better signal source to be sure.
However the captured signal THD does not change much across the tested signal levels, so around -41dB should be the minimum usable dynamic range.

The noise floor also looks quite lively with interesting peaks.
Where they come from is unclear, but the card was put into a regular intel i5 office PC, unshielded.
The strongest peak is at about -60 dB FS, which also limits the usable dynamic range (but not as much as the distortion).

## Performance measurements with [clock generator][clock-gen] at 20, 40 and 50 MHz

To capture two (or more) streams / signals in parallel without clock drift, both CX25800 need to be clocked from the same source.
This rules out each card having their own clock quartz, but needs a single external clock generator.
The data sheet section "3.1.4 Crystal Inputs and Clock Generation" / "Figure 68. Crystal Clock Oscillator" shows this to be possible.
For the purpose of capturing VHS tapes I build an [external clock generator][clock-gen] that is also configurable to different sampling frequencies.

As both my cards were now in sync, clocked from the same source, the following tests mostly focused on comparing the two cards at various settings.
The results between them were quite comparable and so they are only referred to as card0 or card1.
All spectrum plots were created by exporting a 32k fft from audacity as text, then loading into [Libre office][libre-office] and plotting there.

### Silence / noise floor

The following two plots compare both cards at 10 Bit and 8 Bit capture

![](silence-40mhz-20msps-10bit.png)

![](silence-40mhz-40msps-8bit.png)

The improved bit depth also improves the noise floor, also both cards perform rather similarly with only minor differences.

The next plots compares one card at various sample rates, at 10 Bit and 8 Bit capture.

![](silence-10bit-card0.jpg)

![](silence-8bit-card0.png)

The noise floor is clearly the best at 40 MHz, with the other two rates yielding around 10 dB worse results (depending on the spectral location).

### Sine 10kHz


The following three plots compare both cards at 10 Bit with a 10 kHz sine wave input.
As stated before the THD of the Joy-IT JDS2915 function generator used is unclear, and so the about -40 dB difference to the 1st harmonic should not be taken as a definit THD of the CX cards.

![](sine10khz-20mhz-10msps-10bit.png)

![](sine10khz-40mhz-20msps-10bit.png)

![](sine10khz-50mhz-25msps-10bit.png)

The noise floor of card1 is a bit worse here than card0 (by about 5 dB), but otherwise the results are quite comparable again.

However similar to the previous silence / noise floor test, the performance is again best at 40 MHz:

![](sine10khz-10bit-card0.png)

And finally directly comparing 10 bit vs 8 bit on one card at 40 MHz, again shows the lowered noise floor with increased bit depth.

![](sine10khz-40mhz-card0-10bit-vs-8bit.png)

### Conclusions / summary with [clock generator][clock-gen] at 20, 40 and 50 MHz

The input clock frequency to the CX25800 has significant impact on the achieved noise floor.
One might have expected to get better noise floor with lower sample rates, but it is not 20 MHz but 40 MHz that yielded the best results.
The results are improved independent of 10 Bit or 8 Bit sampling.
A practical application is to always sample with 40 MHz, and down-sample in SW (e.g. with [sox][sox-wiki] on-the-fly) to get the best results.
That being said, the noise floor is at about -60dB for all sample rates, across most of the spectrum, which is the quantization noise at 10 Bit.
So even though the noise floor could be better it should realistically not impact the usable range of bits.

The CX25800 cards do feature a 10 Bit ADC, but with the current CXADC driver and knowledge of how these cards work, it is not possible to get the raw 10 Bit samples (as of Oct 2023).
Instead there is some kind of "filtering" done which reduces the sample rate by 1/2 when capturing 10 Bits.
A capture at 10 Bit does seem to yield more resolution than an 8 Bit capture with a better noise floor.
So if increased resolution and a lowered noise floor is more desirable than faster sample rates, the 10 Bits mode is worth a try.


## 10 Bit "Filtered VBI data"

The following test aims to shed light on the CX25800 cards "Filtered VBI data" mode.
The setup was the Joy-IT JDS2915 function generator doing a sine sweep from 1 MHz to 15 MHz (the limit of the function generator).
Then the [clock generator][clock-gen] was used to do two captures:
- 20 MHz clock input, with 8 Bit output at 20 MSps (clock/1)
- 40 MHz clock input, with 10 Bit output at 20 MSps (clock/2)

The 15 MHz maximum output of the sine sweep is 1.5 times the nyquist frequency of a 20MSps capture, and so should show aliasing.
The following image shows the spectrum / waterfall of the two captures side-by-side:

![](10bit-filtering-compare.png)

Aliasing occurs as expected in the 8 bit mode, but also quite visibly in the 10 bit mode.
Both plots were done in audacity with the same settings (+0 db, 512 fft window size, blackman-harris) and nicely show the improved noise floor in the 10 bit capture.

To better see how much attenuation the "filtering" achives, a (logarithmic) time view is better:

![](10bit-filtering-attenuation.png)

Looking at the absolute numbers, 15MHz is attenuated by about 17 dB compared to 10MHz in the 10 bit mode.
So the filtering does reduce aliasing by quite a bit (best seen in the 1st harmonic).
Taking into account that this is implemented in 20 years old comodity hardware, it is a rather good result.
So by using the 10 bit mode, you will get a couple dBs additional headroom to harmonics at x1.5 sample rate.
Depending on your application, the additional resolution may be worth the trade off, to 8 bit mode and a filter in SW that runs with full clock frequency and steeper filtering curve.

Whether there are further steps to this "filtering" besides an anti-aliasing / low-pass filter, cannot be determined from these results.

## [CXADC][cxadc] `center_offset`

This parameter adjusts the center offset of the ADC, and has a range from 0 to 255.
The following image shows a parameter sweep, in steps of 5, about 250ms per value (captured at 40MHz / 20 MSps / 10 bit).

![](center-offset-overview.png)

The numbers in the top part are the values of `center_offset`.
The middle part shows the spectrum overview of the signal 1kHz to 10MHz.
And the bottom part zooms in on the top end of the spectrum at 9.9Mhz zo 10MHz.

The offset values from 0 to 60, and 255, seem to be usable, shifting the DC point as expected.
65 seems to drop the signal and yield a constant value (most negative / lowest 16 bit value).
70 to 125 seem to not change the DC offset.
130 to 250 have some effect on DC, but also cause a very high frequency noise to appear (close to 10MHz, see bottom zoomed in part).

155 causes the DC offset to jump between two values, with a period of about 11 ms, or around 91 Hz.
The following image shows the 155 value zoomed in:

![](center-offset-155-details.png)

At least for this card, 255 and 0-60 seem to be usable.
The optimum for this card is somewhere between 0 and 2.

## [CXADC][cxadc] `level`

This parameter adjusts the gain of a preamplifier before the ADC, and has a range from 0 to 31.
The following image shows a parameter sweep, in steps of 1, about 250ms per value (captured at 40MHz / 20 MSps / 10 bit).
No external signal was present, only the noise floor was captured.

![](level-sweep-spectrum.png)

With increasing amplification, the noise floor also rises.
The next diagram shows a more detailed look, directly comparing the silence spectrum of `level` at 0, 8, 16 and 28.

![](level-sweep-spectrum-compare.png)

The general components of the spectrum seem to be unaffected by the amplifier gain.
The noise floor rises by about 10 db, but the stray peaks by about 15 db, from 0 to 28.
This does suggest that those stray peaks are coupled into the ADC path before the amplifier.

Next the same parameter sweep but this time with a 300 mV (peak-to-peak) 8 MHz sine wave at the input of the CX card (captured at 40MHz / 40 MSps / 8 bit).
8 MHz was chosen to represent the high end of the VHS tape format, and to check if the preamplifier is limiting the bandwidth at higher gain levels.
Using this sine sweep, the THD and gain can be calculated for each level.
Keep in mind that the used Joy-IT JDS2915 function generator probably limits this measurement to around 42 dB.

![](level-sweep-8mhz.png)

With level 0 the THD is possibly limited by the low 300 mV input at 8 bit, higher levels all achieve close to, or above 40 dB THD.
Also the gain has a linear relationship to the level parameter (0.5 dB per level), which suggests that the amplifier bandwidth is sufficient even at higher gain settings.
The following table contains the calculated results:

| level | THD in dB | gain in dB|
|-------|-----------|-----------|
|  0    | 35.5      | 0         |
|  2    | 40.7      | 1.1       |
|  4    | 40.3      | 2.3       |
|  6    | 39.8      | 3.4       |
|  8    | 38.2      | 4.6       |
| 10    | 38.3      | 5.7       |
| 12    | 42.2      | 6.9       |
| 16    | 41.4      | 9.2       |
| 20    | 43        | 11.5      |
| 24    | 43.1      | 13.8      |
| 28    | 43.3      | 16        |
| 31    | 43.8      | 17.7      |

At least for the card that was tested here, the preamplifier performs well, and produces quite usable results.

## [CXADC][cxadc] `tenxfsc`

This parameter controls a resampler. For more details have a look at the [CXADC project documentation][cxadc], but here in particular 3 settings are of interest:
- Value 0, which gives a 1.0 factor (i.e. no resampling)
- Value 1, which gives an 1.24 upsampling factor 
- Value 2, which gives an 1.4 upsampling factor

The following image shows a sine sweep from 1-15MHz (with the usual Joy-IT JDS2915 function generator).
Captured with 40 MHz clock input, in 10Bit mode, with 1.24x and 1.4x upsampling.

![](upsampling-10bit.png)

This sweep clearly shows the nyquist reflection above 10 Mhz, meaning the upsampling occurs *before* the 1/2 downsample from the "Filtered VBI data" mode.
Just to confirm this upsampling is in fact done after the ADC, and is not using some kind of PLL, here's the same sine sweep on a 20 Mhz clock with 1.24x and 8Bit mode.

![](upsampling-8bit.png)

The 8 Bit capture nicely shows the nyquist reflection at 10 MHz, which should be at 12.4 MHz if a PLL was used to drive the ADC with 1.24x clock.
It also shows the upsampling to not be particularly good as it shows the captured signal being mirrored above 10 Mhz.

## A picture of the acquisition system

Using the above gained insights, a rough diagram of the acquisition system of a [CXADC][cxadc] card, should look like this.

![](acquisition-system.png)

## THD measurements with a PC sound card

As the Joy-IT JDS2915 function generator does most likely limit the THD measurements to about 42 dB, another signal source is needed to get better results.
The PC that currently houses both CX25800 cards, features a Realtek ALC3220 as an Intel HDA codec.
There does not seem to be a data sheet or other public information available on this specific chip, but [other Realtek HDA audio codecs outperform 42 dB THD](https://www.realtek.com/en/products/computer-peripheral-ics/item/alc892).
The ALC3220 is capable of running in 192kSps at 16 bit, which should be sufficient to produce a "clean" enough 10 kHz sine signal.

The measurement setup was a direct connection from the sound card output to the 75 Ohms terminated CX input.
The Rigol DS1054Z was connected as well to monitor for output clipping of the sound card.
Playing back a full scale 10kHz 16 bit sine wave, the output does indeed clip quite a bit if the output volume is maxed out.
Next I lowered the sound card output volume (`alsamixer`) until no clipping was visible.
To make sure the output was in a safe range, I lowered the output by an additional 2 dB.
This gave an about 500 mVpp output into the 75 Ohm termination.
To maximize the used CX ADC range, `level` set to 16 gave about -5 dB full scale signal.
The signal was captured at 40MHz clock, in both 10 bit 20 MSps and 8 bit 40 MSps:

![](sine10khz-alc3220-card0-40mhz.png)

The above diagram shows the spectrum of these captures.
In both cases the THD is 59 dB, with the 8th harmonic (90kHz) being the highest.

This test was repeated for 20 MHz, 28.6 MHz and 50 MHz clock, in both 10 bit and 8 bit.
The following table shows the results:

| clock    | sampling         | 10 kHz THD | strongest harmonic |
|----------|------------------|------------|--------------------|
| 20 MHz   | 10 MSps 10 Bit   | 54 dB      | 4th / 50 kHz       |
| 20 MHz   | 20 MSps  8 Bit   | 54 dB      | 4th / 50 kHz       |
|*28.6 MHz*| 14.3 MSps 10 Bit | *47 dB*    | 4th / 50 kHz       |
|*28.6 MHz*| 28.6 MSps  8 Bit | *47 dB*    | 4th / 50 kHz       |
|**40 MHz**| 20 MSps 10 Bit   | **59 dB**  | 8th / 90 kHz       |
|**40 MHz**| 40 MSps  8 Bit   | **59 dB**  | 8th / 90 kHz       |
| 50 MHz   | 25 MSps 10 Bit   | 41 dB      | 4th / 50 kHz       |
| 50 MHz   | 50 MSps  8 Bit   | 41 dB      | 4th / 50 kHz       |

The CX card clearly performs the best at 40 MHz clock rate and worst at 50 MHz.
Results for the 28.6 MHz clock are worse than both 20 MHz and 40 MHz, most likely due to the fractional mode of the [clock generator][clock-gen].
Bit-depth / "filtering" does not affect the results.

So in conclusion, 50 MHz clock rate is possibly close to the cards limits and should only be used if the additional bandwidth is needed.
The optimal performance is reached with 40 MHz, and the 8 or 10 bit mode can be chosen based on source signal requirements.
If lower sampling rates are desired, it is best to sample with a 40 MHz clock and then downsample in software (e.g. [`sox`][sox-wiki]) to get the best result.

## Future stuff

The THD measurements should be repeated with a better signal generator to actually judge how good/bad the CX25800 cards THD really is at higher frequencies (e.g. 1 MHz and 5 MHz).

## Abbreviations

- ADC - Analog-to-digital converter
- AGC - Automatic gain control
- DSO - Digital storage oscilloscope
- FFT - Fast Fourier Transform
- Intel HDA - Intel High Definition Audio
- PCI  - Peripheral Component Interconnect
- PCIe - Peripheral Component Interconnect Express
- RF - Radio Frequency
- THD - Total harmonic distortion
- VBI - Vertical blanking interval 
- VHS - Video Home System

[cxadc]: https://github.com/happycube/cxadc-linux3/
[clock-gen]: https://gitlab.com/wolfre/cxadc-clock-generator-audio-adc
[libre-office]: https://www.libreoffice.org/
[sox-wiki]: https://en.wikipedia.org/wiki/SoX
