#!/bin/bash

CARD=cxadc0
VMUX=0           # 0=bnc
LEVEL=0          # 0 min gain ... 31 max gain
SIXDB=0          # 0 off 1 +6db
CRYSTAL=28636360 # 28.6 MHZ
FSCX=0           # 0=1.0  1=1.24  2=1.4
TEN=1            # 0= 8bit  1=10bit (half rate)


function die
{
	echo "$@" >&2
	exit 1
}

function sample_rate
{
	local f=0
	if [[ $FSCX == 0 ]] ; then f=100 ; fi
	if [[ $FSCX == 1 ]] ; then f=124 ; fi
	if [[ $FSCX == 2 ]] ; then f=140 ; fi

	if [[ $f == 0 ]] ; then die "Unkonwn FSCX $FSCX" ; fi

	f=$(( $f * $CRYSTAL ))
	f=$(( $f / 100 ))

	if [[ $TEN != 0 ]] ; then 
		f=$(( $f / 2 ))
	fi

	echo -n $f
}

function file_ext
{
	if [[ $TEN == 0 ]] ; then
		echo -n u8
	else
		echo -n u16
	fi
}

SYSFS_DIR=/sys/class/cxadc/$CARD/device/parameters

echo $VMUX  > $SYSFS_DIR/vmux     || die "vmux"
echo $SIXDB > $SYSFS_DIR/sixdb    || die "sixdb"
echo $LEVEL > $SYSFS_DIR/level    || die "level"
echo $FSCX  > $SYSFS_DIR/tenxfsc  || die "tenxfsc"
echo $TEN   > $SYSFS_DIR/tenbit   || die "tenbit"


file="/media/capture/raw/capture-$(( $(sample_rate) / 1000))khz-m${VMUX}g${LEVEL}-$(date +%Y%m%d%H%M%S).$(file_ext)"
echo "Capturing to '$file'"
echo "Press ctrl+c to stop"
cat /dev/$CARD | pv --timer --rate --bytes --buffer-size 256m > $file
